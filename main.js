console.log('add')
function add(num1, num2) {
    return num1 + num2
}
console.log(add(2, 4));

console.log('multiply')
function multiply(num1, num2) {
    let result = 0;
    for (let i = 0; i < num2; i++) {
        result = add(result, num1)
    }
    return result;
}
console.log(multiply(6, 8))

console.log('power')
function power(x, n) {
    let result = 1;
    for (let i = 0; i < n; i++) {
        result = multiply(x, result)
    }
    return result;
}
console.log(power(2, 8))

console.log('factorial')
function factorial(y) {
    let result = 1;
    for (let i = y; i > 1; i--) {
        result = multiply(i, result)
    }
    return result;
}
console.log(factorial(4))

console.log('fibonacci')
function fibonacci(n) {
    let fib0 = 0; let fib1 = 1; let result = 0;
    if (n < 1) {
        console.log("invalid number")
    } else if (n == 1) {
        return fib0;
    } else if (n == 2) {
        return fib1;
    } else {
        for (let i = 0; i < n - 2; i++) {
            result = add(fib0, fib1)
            fib0 = fib1;
            fib1 = result;
        }
        return result;
    }
}
console.log(fibonacci(8))
